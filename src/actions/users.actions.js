import {
    FETCH_USERS_ERROR,
    FETCH_USERS_PENDING,
    FETCH_USERS_SUCCESS,
    USERS_PAGINATION_CHANGE
} from "../constants/users.constants";

export const changePagination = (page) => {
    return {
        type: USERS_PAGINATION_CHANGE,
        page: page
    }
}

export const fetchUsers = (limit, currentPage) => {
    return async ( dispatch ) => {

        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        await dispatch({
            type: FETCH_USERS_PENDING
        });

        try {
            const responseTotalUser = await fetch("https://jsonplaceholder.typicode.com/users", requestOptions);

            const dataTotalUser = await responseTotalUser.json();

            const response = await fetch("https://jsonplaceholder.typicode.com/users?_start=" + ((currentPage-1) * limit) + "&_limit=" + limit, requestOptions);

            const data = await response.json();

            return dispatch({
                type: FETCH_USERS_SUCCESS,
                totalUser: dataTotalUser.length,
                data: data
            })
        } catch (err) {
            return dispatch({
                type: FETCH_USERS_ERROR,
                error: err
            })
        }
    }
}